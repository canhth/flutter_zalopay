import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:intl/intl.dart';

import 'zalopay_config.dart';

/// Format DateTime to String with format string
String formatDateTime(DateTime dateTime, String format) {
  return DateFormat(format).format(dateTime).toString();
}

String getAppTransId() => formatDateTime(DateTime.now(), "yyMMdd_hhmmss");

String getDescription(String appTransId) {
  return "Merchant Demo - Thanh toán đơn hàng  #$appTransId";
}

String getBankCode() => "JCB";

String getMacCreateOrder(String data) {
  final key = utf8.encode(ZaloPayConfig.key1);
  final bytes = utf8.encode(data);

  final hmacSha256 = Hmac(sha256, key);
  return hmacSha256.convert(bytes).toString();
}

String getMacOrderStatus(String data) {
  final key = utf8.encode(ZaloPayConfig.key1);
  final bytes = utf8.encode(data);

  final hmacSha256 = Hmac(sha256, key);
  return hmacSha256.convert(bytes).toString();
}
