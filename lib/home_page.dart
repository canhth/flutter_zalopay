import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_zalopay/models/order_status_response.dart';
import 'package:logger/logger.dart';

import 'models/create_order_response.dart';
import 'repos/zalopay_payment.dart';

String appTransId = "";

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final logger = Logger();

  static const MethodChannel payOrderChannel =
      MethodChannel('com.example.flutter_zalopay/payOrder');

  bool orderCreated = false;
  String zpTransToken = "";

  int payAmount = 1000;
  String orderResult = "";

  bool showResult = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Zalopay payment demo"),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FilledButton(
              onPressed: () => _createOrder(payAmount),
              child: const Text('Create order'),
            ),
            showOrderInfo(),
            FilledButton(
              onPressed: orderCreated ? () => _payOrder(zpTransToken) : null,
              child: const Text('Pay order'),
            ),
            FilledButton(
              onPressed: orderCreated
                  ? () {
                      _getOrderStatus(appTransId: appTransId);
                    }
                  : null,
              child: const Text('get order status'),
            ),
            showOrderStatus(),
          ],
        ),
      ),
    );
  }

  Padding showOrderStatus() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Visibility(
        visible: showResult,
        child: Text(
          "Orders status: $orderResult",
          style: const TextStyle(fontSize: 24),
        ),
      ),
    );
  }

  Padding showOrderInfo() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Visibility(
        visible: showResult,
        child: Text(
          "zpTransToken: $zpTransToken\nappTransId: $appTransId",
          style: const TextStyle(fontSize: 18),
        ),
      ),
    );
  }

  Future<void> _createOrder(int payAmount) async {
    showDialog(
      context: context,
      builder: (context) => const Center(
        child: CircularProgressIndicator(),
      ),
    );

    try {
      if (payAmount < 1000 && payAmount > 1000000) {
        setState(() {
          zpTransToken = "Invalid amount";
        });
      } else {
        final CreateOrderResponse? result = await createOrder(payAmount);

        if (result != null) {
          setState(() {
            zpTransToken = result.zpTransToken;
            showResult = true;
            orderCreated = true;
          });
        }
      }
    } catch (e) {
      logger.e("Create order Error:${e.toString()}");
    } finally {
      if (mounted) {
        Navigator.pop(context);
      }
    }
  }

  Future<void> _payOrder(String zptoken) async {
    String response = "";

    try {
      logger.i('start pay order | zpToken: $zptoken');
      final String result = await payOrderChannel.invokeMethod(
        'payOrder',
        {"zptoken": zptoken},
      );
      response = result;
      logger.i('pay order result: $result');
    } catch (e) {
      logger.e("Failed to Invoke: ${e.toString()}");
      response = "Thanh toán thất bại";
    } finally {
      setState(() {
        orderResult = response;
      });
    }
  }

  Future<void> _getOrderStatus({required String appTransId}) async {
    logger.d("start get order status: appTransId: $appTransId");
    final OrderStatusResponse? result = await getOrderStatus(
      appTransId: appTransId,
    );

    if (result != null) {
      setState(() {
        if (result.returnMessage.isEmpty) {
          final data = {
            '1': "Thành công",
            '2': "Thất bại",
            '3': "Đơn hàng chưa thanh toán hoặc giao dịch đang xử lý",
          };
          orderResult = data[result.returnCode.toString()] ?? 'Empty';
        } else {
          orderResult = result.returnMessage;
        }
      });
      logger.i("Order status: ${result.returnMessage}");
    }
  }
}
