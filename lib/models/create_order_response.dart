/// Docs: https://docs.zalopay.vn/v2/general/overview.html#tao-don-hang_tham-so-api-tra-ve
class CreateOrderResponse {
  final int returnCode;
  final String returnMessage;
  final int subReturnCode;
  final String subReturnMessage;
  final String orderUrl;
  final String zpTransToken;

  CreateOrderResponse({
    required this.returnCode,
    required this.returnMessage,
    required this.subReturnCode,
    required this.subReturnMessage,
    required this.orderUrl,
    required this.zpTransToken,
  });

  factory CreateOrderResponse.fromJson(Map<String, dynamic> json) {
    return CreateOrderResponse(
      returnCode: json['return_code'] as int,
      returnMessage: json['return_message'] as String,
      subReturnCode: json['sub_return_code'] as int,
      subReturnMessage: json['sub_return_message'] as String,
      orderUrl: json['order_url'] as String,
      zpTransToken: json['zp_trans_token'] as String,
    );
  }
}
