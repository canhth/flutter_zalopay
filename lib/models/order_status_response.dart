/// Docs: https://docs.zalopay.vn/v2/general/overview.html#truy-van-trang-thai-thanh-toan-cua-don-hang_dac-ta-api_tham-so-api-tra-ve
class OrderStatusResponse {
  final int returnCode;
  final String returnMessage;
  final int subReturnCode;
  final String subReturnMessage;
  final bool isProcessing;
  final int amount;
  final int zpTransId;

  const OrderStatusResponse({
    required this.returnCode,
    required this.returnMessage,
    required this.subReturnCode,
    required this.subReturnMessage,
    required this.isProcessing,
    required this.amount,
    required this.zpTransId,
  });

  Map<String, dynamic> toJson() {
    return {
      'return_code': returnCode,
      'return_message': returnMessage,
      'sub_return_code': subReturnCode,
      'sub_return_message': subReturnMessage,
      'is_processing': isProcessing,
      'amount': amount,
      'zp_trans_id': zpTransId,
    };
  }

  factory OrderStatusResponse.fromJson(Map<String, dynamic> json) {
    return OrderStatusResponse(
      returnCode: json['return_code'] as int,
      returnMessage: json['return_message'] as String,
      subReturnCode: json['sub_return_code'] as int,
      subReturnMessage: json['sub_return_message'] as String,
      isProcessing: json['is_processing'] as bool,
      amount: json['amount'] as int,
      zpTransId: json['zp_trans_id'] as int,
    );
  }
}
