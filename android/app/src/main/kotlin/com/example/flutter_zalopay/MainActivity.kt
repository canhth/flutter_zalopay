package com.example.flutter_zalopay

import android.content.Intent
import android.os.Bundle

import io.flutter.Log
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import vn.zalopay.sdk.Environment
import vn.zalopay.sdk.ZaloPayError
import vn.zalopay.sdk.ZaloPaySDK
import vn.zalopay.sdk.listeners.PayOrderListener


class MainActivity : FlutterActivity() {
    // Merchant AppID For test
    private val appID: Int = 554
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ZaloPaySDK.init(appID, Environment.SANDBOX)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        Log.d("newIntent", intent.toString())
        ZaloPaySDK.getInstance().onResult(intent)
    }

    private val channelPayOrder: String = "com.example.flutter_zalopay/payOrder"
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger, channelPayOrder
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "payOrder" -> {
                    payOrder(call, result)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    private fun payOrder(call: MethodCall, result: MethodChannel.Result) {
        try {
            val token = call.argument<String>("zptoken")
            val merchantAppDeeplink = "demozpdk://app"

            ZaloPaySDK.getInstance()
                .payOrder(this, token!!, merchantAppDeeplink, object : PayOrderListener {
                    override fun onPaymentCanceled(zpTransToken: String?, appTransID: String?) {
                        //Handle User Canceled
                        result.success("❌❌❌ User Canceled")
                    }

                    override fun onPaymentError(
                        zaloPayErrorCode: ZaloPayError?,
                        zpTransToken: String?,
                        appTransID: String?,
                    ) {
                        //Redirect to Zalo/ZaloPay Store when zaloPayError == ZaloPayError.PAYMENT_APP_NOT_FOUND
                        //Handle Error
                        result.success("❌❌❌ Payment failed")
                    }

                    override fun onPaymentSucceeded(
                        transactionId: String,
                        transToken: String,
                        appTransID: String?,
                    ) {
                        //Handle Success
                        result.success("✅✅✅✅ Payment Success")
                    }
                })
        } catch (e: Exception) {
            result.success("❌❌❌ Payment failed: ${e.message}")
        }

    }
}
